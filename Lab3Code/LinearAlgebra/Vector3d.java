/*Louisa Yuxin Lin 1933472*/
package LinearAlgebra;
public class Vector3d{
   private double x;
   private double y;
   private double z; 
   public Vector3d(double x,double y, double z){
       this.x=x;
       this.y=y;
       this.z=z;
   }
   public double getX(){
       return this.x;
   }
   public double getY(){
    return this.y;
}
    public double getZ(){
    return this.z;
    }
    public double getMagnitude(){
        return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)+Math.pow(this.z,2));
    }
    public double dotProduct(Vector3d v){
        double result=0;
        result=(this.x*v.x)+(this.y*v.y)+(this.z*v.z);
        return result;
    }
    public Vector3d add(Vector3d v){
        Vector3d n= new Vector3d(0,0,0);
        n.x=this.x+v.x;
        n.y=this.y+v.y;
        n.z=this.z+v.z;
        return n;
    }
}

