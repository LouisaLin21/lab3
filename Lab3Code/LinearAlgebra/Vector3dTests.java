/*Louisa Yuxin Lin 1933472*/
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class Vector3dTests{
	//This test make sure the get methods return the correct results
	@Test 
	public void testGetpara(){
		
		Vector3d v = new Vector3d(4,5,6);
		assertEquals(v.getX(),4);
		assertEquals(v.getY(),5);
		assertEquals(v.getZ(),6);
		
	}
	//This method make sure the magnitude method works correctly
	@Test 
	public void testMagnitude(){
		
		Vector3d v = new Vector3d(4,5,6);
		assertEquals(v.getMagnitude(),8.774964387392123,0.00000000000001);
	}
	//This method tests the dot product of the two vectors.
	@Test 
	public void testDB(){
		Vector3d v = new Vector3d(4,5,6);
		Vector3d w = new Vector3d(7,8,9);
		assertEquals(v.dotProduct(w),122);
	}
	//This method tests the adding method
	@Test 
	public void testAdd(){
		
		Vector3d v = new Vector3d(4,5,6);
		Vector3d w = new Vector3d(7,8,9);
		Vector3d result =v.add(w);
		assertEquals(result.getX(),11);
		assertEquals(result.getY(),13);
		assertEquals(result.getZ(),15);
		
	}
}
